package com.hectorcode.springApplication.student;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

// Responsible for dependency injection
@Repository
public interface StudentRepository extends JpaRepository<Student, Long> {

    @Query("SELECT * FROM Student as s where s.email = ?1")
    Optional<Student> findStudentByEmail(String email);

}
