package com.hectorcode.springApplication.student;

import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Configuration;

import java.time.LocalDate;
import java.time.Month;
import java.util.List;

@Configuration
public class StudentConfig {

    CommandLineRunner commandLineRunner(StudentRepository repository) {
        return args -> {
            Student mariam = new Student(
                    "Mariam",
                    "mariam.jamal@gmail.com",
                    LocalDate.of(2000, Month.JANUARY, 23));

            Student alex = new Student(
                    "Alex",
                    "alex.hudson@gmail.com",
                    LocalDate.of(1999, Month.MARCH, 3));

            repository.saveAll(List.of(mariam, alex));
        };
    }
}
